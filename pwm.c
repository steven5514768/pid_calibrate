#include "pwm.h"
#include "embARC.h"
#include "embARC_debug.h"


ADC_DEFINE(adc_test, ADC_INT_NUM, ADC_CRTL_BASE, NULL);

void arduino_pin_init(void)
{
	io_arduino_config(ARDUINO_PIN_3, ARDUINO_PWM, IO_PINMUX_ENABLE);//pwm timer ch0
	io_arduino_config(ARDUINO_PIN_5, ARDUINO_PWM, IO_PINMUX_ENABLE);//pwm timer ch1
	io_arduino_config(ARDUINO_PIN_6, ARDUINO_PWM, IO_PINMUX_ENABLE);//pwm timer ch2
	io_arduino_config(ARDUINO_PIN_9, ARDUINO_PWM, IO_PINMUX_ENABLE);//pwm timer ch3
	io_arduino_config(ARDUINO_PIN_10, ARDUINO_PWM, IO_PINMUX_ENABLE);//pwm timer ch4
	io_arduino_config(ARDUINO_PIN_11, ARDUINO_PWM, IO_PINMUX_ENABLE);//pwm timer ch5
}


void pwm_increase(int channel)
{
    int output;
    
	DEV_PWM_TIMER_PTR pwm_timer_test = pwm_timer_get_dev(DW_PWM_TIMER_0_ID);
	pwm_timer_test->pwm_timer_open();

   	for(output=0;output<100;output++){
		board_delay_ms(7, 1);
		pwm_timer_test->pwm_timer_write(channel, DEV_PWM_TIMER_MODE_PWM, 32000, output);
		EMBARC_PRINTF("Output = %d\n",output);
	}
}
void pwm_decrease(int channel)
{
    int output;
    
	DEV_PWM_TIMER_PTR pwm_timer_test = pwm_timer_get_dev(DW_PWM_TIMER_0_ID);
	pwm_timer_test->pwm_timer_open();

    for(output=100;output>0;output--){
	board_delay_ms(7, 1);
	pwm_timer_test->pwm_timer_write(channel, DEV_PWM_TIMER_MODE_PWM, 32000, output);
	EMBARC_PRINTF("Output = %d\n",output);
	}
}

void pwm_set(int channel,int output)
{
	
	DEV_PWM_TIMER_PTR pwm_timer_test = pwm_timer_get_dev(DW_PWM_TIMER_0_ID);
	pwm_timer_test->pwm_timer_open();
	pwm_timer_test->pwm_timer_write(channel, DEV_PWM_TIMER_MODE_PWM, 32000, output);
	EMBARC_PRINTF("Channel = %d Output = %d\n",channel,output);
}