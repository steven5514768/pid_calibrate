
#include "embARC.h"
#include "embARC_debug.h"
#include "mpu9250.h"
#include "stdio.h"
#include "header.h"

int main(void)
{
    float goal_pitch=0;
    float goal_roll=0;
    float goal_yaw=0;
    arduino_pin_init();
   pwm_set(0,16);
   pwm_set(1,16);
   pwm_set(2,16);
   pwm_set(3,16);
   for(int i=0;i<100;i++)
   {
       board_delay_ms(100, 1);
   } 
    

    loiter_t loiter_parameter;
    loiter_set_parameter(&loiter_parameter,goal_pitch,goal_roll,goal_yaw);
    for(int i=0;i<2000;i++)
    {
       loiter(&loiter_parameter,i);
       //notice: The function will use Timer1 to calibrate the yaw, so don't use Timer1 when using this function. 
        board_delay_ms(10, 1); 
    }
   // printf("t : %d\r\n", loiter_parameter.t);
    
	for(int k=0;k<loiter_parameter.t;k++)
	{
		printf("%d\n",loiter_parameter.i_1[k]);
	
	}
    
    /*
   //pwm_set(0,25);
   for(int i=0;i<50;i++)
   {
       board_delay_ms(100, 1);
   }*/
    pwm_set(0,16);
   pwm_set(1,16);
   pwm_set(2,16);
   pwm_set(3,16);
    return E_SYS;
}

