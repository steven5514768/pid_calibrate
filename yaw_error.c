#include "stdio.h"
#include "embARC.h"
#include "embARC_debug.h"
#include "yaw_error.h"

#define calibrate 0.265
//#define calibrate 0.545


void yaw_error_count_start(void)
{
    timer_int_clear(TIMER_1);
    timer_stop(TIMER_1);
	timer_start(TIMER_1, TIMER_CTRL_IE, BOARD_CPU_CLOCK*1000);
}

/*uint64_t real_timer_ms(void)
{
	uint64_t val;
	uint64_t out;
	timer_current(TIMER_1, &val);
	val /= 144000;
    out = val;
	return out;
}
*/
/*uint64_t real_timer_s(int TIMER)
{
	uint64_t val;
	timer_current(TIMER, &val);
	val /= 144000000;

	return val;
}
*/
// yaw error calibrate

void yaw_calibrate(float *error, uint64_t *time, uint64_t *pre) 
{
	if (*pre != *time) {
			*error += calibrate;
			*pre = *time;
	}

	return;
}

